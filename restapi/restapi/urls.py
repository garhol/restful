from django.conf.urls import patterns, include, url
from tastypie.api import Api
from restapi.apiserv.api import ApiDataResource, UserResource, GenerateUser, CreateApiDataResource

v1_api = Api(api_name='v1')
v1_api.register(ApiDataResource())
v1_api.register(UserResource())
v1_api.register(GenerateUser())
v1_api.register(CreateApiDataResource())

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(v1_api.urls)),
    
)
