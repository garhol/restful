from django.contrib.auth.models import User
from django.db import models

from tastypie import fields

from tastypie.authorization import Authorization
from tastypie.authentication import ApiKeyAuthentication

from tastypie.resources import ModelResource
from tastypie.models import create_api_key, ApiKey

from tastypie.exceptions import TastypieError

from models import ApiData

models.signals.post_save.connect(create_api_key, sender=User)


class GenerateUser(ModelResource):

    """
    GenerateUser an api user with a unique key
    This user will be required to add or read key/value pairs
    POST method for simplicity of sending data
    """

    class Meta:
        object_class = User
        include_resource_uri = False
        resource_name = 'reg'
        allowed_methods = ['post']
        queryset = User.objects.all()
        always_return_data = True

    def obj_create(self, bundle, request=None, **kwargs):
        username, password = bundle.data['username'], bundle.data['password']
        try:
            bundle.obj = User.objects.create_user(username, '', password)
            bundle.obj.save()
        except TastypieError:
            raise TastypieErrorTastypieError('That seat\'s taken - pick another username')
        
        # grab the api key and return to the user with the rest of the response
        api = ApiKey.objects.get(user__id=bundle.obj.id)
        bundle.data['api_key'] = str(api.key)

        return bundle


class UserResource(ModelResource):

    """
     UserResource - returns a list of users. // for testing/verification purposes only
     
     *************
     ***  Should not be pushed into production for obvious reasons.
     *************
     
     GET method for retrieving data
    """
    class Meta:
        queryset = User.objects.all()
        excludes = ['email', 'password',
                    'is_active', 'is_staff', 'is_superuser']
        resource_name = 'user'
        allowed_methods = ['get']


class ApiDataResource(ModelResource):

    """
     ApiDataResource - returns a list of apidata key/value pairs available to the user
     GET method for retrieval.
    """
    user = fields.ForeignKey(UserResource, 'apiuser')

    class Meta:
        queryset = ApiData.objects.all()
        resource_name = 'data'
        allowed_methods = ['get']
        authorization = Authorization()
        authentication = ApiKeyAuthentication()

    def get_object_list(self, request):
        try:
            requser = User.objects.get(id=request.user.id)
            return super(ApiDataResource, self).get_object_list(request).filter(apiuser=requser)
        except:
            raise TastypieError('Supply a username and key please')

class CreateApiDataResource(ModelResource):

    """
    CreateApiDataResource - accepts a key/value pair and assigns them to the user
    POST method for sending data
    """

    class Meta:
        queryset = ApiData.objects.all()
        object_class = ApiData
        allowed_methods = ['post']
        authorization = Authorization()
        authentication = ApiKeyAuthentication()
        resource_name = 'add'
        always_return_data = False

    def obj_create(self, bundle, request=None, **kwargs):   
        try:
            return super(CreateApiDataResource, self).obj_create(bundle, apiuser=bundle.request.user)
        except:
            raise TastyPieError('Please supply both key/value and your credentials.')