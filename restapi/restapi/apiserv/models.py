from django.db import models
from django.contrib.auth.models import User


class ApiData(models.Model):
    apiuser = models.ForeignKey(User, null=True)
    keydata = models.CharField(max_length=20)
    valuedata = models.CharField(max_length=100, blank=True)

    def __unicode__(self):
        return self.keydata
