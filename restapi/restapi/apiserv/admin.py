from models import ApiData
from django.contrib import admin

class ApiDataAdmin(admin.ModelAdmin):
    list_display = ('apiuser', 'keydata','valuedata')


admin.site.register(ApiData, ApiDataAdmin)

